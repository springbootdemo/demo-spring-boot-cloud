# Config Server #
Server for external configuration.

See http://projects.spring.io/spring-cloud/spring-cloud.html

Run this project as a Spring Boot app (e.g. import into IDE and run main method, or use "mvn spring-boot:run or gradle bootRun or ./gradlew bootRun"). It will start up on port 8888.

# How to use it yourself #
* Create the config server
* Create a Github repository for the property files. Like https://github.com/wesboe/spring-boot-config
* Add the client application.properties file and rename it to [client service name].properties
* In the client add a bootstrap.properties file
* Add the following properties to the bootstrap.properties file
spring.application.name=[client service name]
spring.cloud.config.uri=http://localhost:8888
* Remove the application.properties file 
 