# Eureka Server #
Server for service discovery.

See http://projects.spring.io/spring-cloud/spring-cloud.html

Run this project as a Spring Boot app (e.g. import into IDE and run main method, or use "mvn spring-boot:run or gradle bootRun or ./gradlew bootRun"). It will start up on port 8761 and serve the Eureka dashboard at /.