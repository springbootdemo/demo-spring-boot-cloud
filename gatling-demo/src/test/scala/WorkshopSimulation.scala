import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._
import scala.util.Random

class WorkshopSimulation extends Simulation {

	val httpProtocol = http
			//.baseURL("http://computer-database.gatling.io") // Here is the root for all relative URLs
			.baseURL("http://localhost:8111")
			.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
			.doNotTrackHeader("1")
			.acceptLanguageHeader("en-US,en;q=0.5")
			.acceptEncodingHeader("gzip, deflate")
			.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

	val scn = scenario("Simulation")
			.repeat(60) {
				 exec(
					http("Request_1")
							.get("/reservation"))
				.pause(1)
			}

	//setUp(scn.inject(constantUsersPerSec(1) during(1 minutes)).protocols(httpProtocol))
	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)

}
