# Hystrix dashboard #

This service stores reservations for flight travels. It has a H2 in memory database to store the reservations.

Run this project as a Spring Boot app (e.g. import into IDE and run main method, or use "mvn spring-boot:run or gradle bootRun or ./gradlew bootRun"). It will start up on port 6677.

## Additional information ##
* Use /h2-console to see stored reservations

## Spring-boot dependencies 1.3.1.RELEASE ##
* web

## Spring-boot cloud dependencies ##
* hystrix-dashboard