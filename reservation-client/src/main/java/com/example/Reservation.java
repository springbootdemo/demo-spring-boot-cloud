package com.example;

import java.util.Date;

public class Reservation {

    public Long reservationNumber;
    public String destination;
    public String departure;
    public Date date;

    public Long getReservationNumber() {
        return reservationNumber;
    }

    public void setReservationNumber(Long reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "reservationNumber=" + reservationNumber +
                ", destination ='" + destination + '\'' +
                ", departure ='" + departure + '\'' +
                ", date ='" + date + '\'' +
                '}';
    }
}
