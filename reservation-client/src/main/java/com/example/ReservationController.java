package com.example;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/reservation")
public class ReservationController {

    @Autowired
    ReservationServiceFeignClient feignClient;

    @RequestMapping(method = { RequestMethod.GET})
    @HystrixCommand(groupKey = "bla.bla", fallbackMethod = "defaultReservations")
    public Iterable<Reservation> getReservations() {
        return feignClient.getReservations();
    }

    @RequestMapping(method = { RequestMethod.POST})
    public void saveReservations(Reservation reservation) {
        feignClient.saveReservation(reservation);
    }

    public Iterable<Reservation> defaultReservations() {
        return null;
    }

}
