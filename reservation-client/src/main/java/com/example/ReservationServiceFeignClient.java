package com.example;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("RESERVATION-SERVICE")
interface ReservationServiceFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/v1/reservation")
    Iterable<Reservation> getReservations();

    @RequestMapping(method = RequestMethod.POST, value = "/v1/reservation", consumes ="application/json")
    Reservation saveReservation(Reservation reservation);}
