package com.example;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/reservations")
public class ReservationWebController  {

    private static final String reader = "craig";

    private ReservationServiceFeignClient feignClient;

    @Autowired
    public ReservationWebController(ReservationServiceFeignClient feignClient) {
        this.feignClient = feignClient;
    }

    @RequestMapping(method=RequestMethod.GET)
    public String readersBooks(Model model) {

        Iterable<Reservation> reservations = feignClient.getReservations();

        if (reservations != null) {
            List<Reservation> list = Lists.newArrayList(reservations);
            model.addAttribute("reservationList", list);
        }
        return "reservations";
    }

    @RequestMapping(method=RequestMethod.POST)
    public String addToReadingList(Reservation reservation) {
        Reservation result = feignClient.saveReservation(reservation);
        return "redirect:/reservations.html";
    }

}
