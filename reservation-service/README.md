# Reservation Server #

This service stores reservations for flight travels. It has a H2 in memory database to store the reservations.

Run this project as a Spring Boot app (e.g. import into IDE and run main method, or use "mvn spring-boot:run or gradle bootRun or ./gradlew bootRun"). It will start up on port 7777.

## API ##
http://localhost:7777/v1/reservation
http://localhost:7777/v1/reservation/destination/AMS

## TODO ##
* Add POST Rest call
* Add get reservations REST call

## Additional information ##
* Use /h2-console to see stored reservations

## Spring-boot dependencies 1.3.1.RELEASE ##
* devtools
* actuator
* security
* web
* eureka
* data-jpa