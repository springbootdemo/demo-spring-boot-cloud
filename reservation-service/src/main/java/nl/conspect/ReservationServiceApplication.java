package nl.conspect;

import nl.conspect.dbRepository.ReservationRepository;
import nl.conspect.domain.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaRepositories
public class ReservationServiceApplication {

	@Autowired
	public ReservationRepository reservationRepository;

	public static void main(String[] args) {
		SpringApplication.run(ReservationServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner line() {
		return args -> {
			Reservation r1 = new Reservation();
			r1.setDeparture("AMS");
			r1.setDestination("NYE");
			reservationRepository.save(r1);

			Reservation r2 = new Reservation();
			r2.setDeparture("AMS");
			r2.setDestination("SYD");
			reservationRepository.save(r2);};
	}

}
