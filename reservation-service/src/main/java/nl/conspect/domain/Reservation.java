package nl.conspect.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Reservation {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public long reservationNumber;

    public String destination;

    public String departure;

    public Date date;

}
