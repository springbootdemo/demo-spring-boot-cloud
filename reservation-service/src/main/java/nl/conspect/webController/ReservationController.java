package nl.conspect.webController;

import nl.conspect.dbRepository.ReservationRepository;
import nl.conspect.domain.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/v1/reservation")
public class ReservationController {

    @Autowired
    public ReservationRepository reservationRepository;

    @RequestMapping(method = {RequestMethod.GET})
    public Iterable<Reservation> getReservations() {
        return reservationRepository.findAll();

    }

    @RequestMapping(value = "/destination/{destination}",method = {RequestMethod.GET})
    public Iterable<Reservation> getReservationByDestination(@PathVariable("destination") String destination) {
        return reservationRepository.findByDestination(destination);

    }

    @RequestMapping(value = "/{reservationNumber}",method = {RequestMethod.GET})
    public Reservation getReservation(@PathVariable("reservationNumber") Long reservationNumber) {
        return reservationRepository.findOne(reservationNumber);
    }

    @RequestMapping(method = {RequestMethod.POST})
    public ResponseEntity<?> saveReservation(@RequestBody Reservation reservation) {

        Reservation result = reservationRepository.save(reservation);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{reservationNumber}")
                .buildAndExpand(result.getReservationNumber()).toUri());
        return new ResponseEntity<>(result, httpHeaders, HttpStatus.CREATED);
    }

}
