package nl.conspect;

import nl.conspect.dbRepository.ReservationRepository;
import nl.conspect.domain.Reservation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReservationServiceApplication.class)
@WebAppConfiguration
public class ReservationRepoApplicationTests {

	@Inject
	public ReservationRepository reservationRepository;

	@Test
	public void contextLoads() {
	}

	@Before
	public void init() {
		Reservation reservation = new Reservation();
		reservation.setDeparture("NYE");
		reservation.setDestination("AMS");
		reservationRepository.save(reservation);

		Reservation reservationBack = new Reservation();
		reservationBack.setDeparture("NYE");
		reservationBack.setDestination("AMS");
		reservationRepository.save(reservationBack);
	}


	@Test
	public void findByDestinationTest() {

		Iterable<Reservation> result = reservationRepository.findByDestination("NYE");
		result.forEach(r -> System.out.println(r.toString()));

	}

}
